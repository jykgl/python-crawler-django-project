from django.conf.urls import url
from . import views

app_name = 'app'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^book_search/$', views.book_search, name='book_search'),
    url(r'^pie_graph/$', views.pie_graph, name='pie_graph'),
    url(r'^bar_chart/$', views.bar_chart, name='bar_chart'),
    url(r'^bar_chart_2/$', views.bar_chart_2, name='bar_chart_2'),
    url(r'^publish_house_info_1/$', views.publish_house_info_1, name='publish_house_info_1'),
    url(r'^publish_house_info_2/$', views.publish_house_info_2, name='publish_house_info_2'),
    url(r'^publish_house_info_3/$', views.publish_house_info_3, name='publish_house_info_3'),
    url(r'^publish_search/$', views.publish_search, name='publish_search'),
    ]