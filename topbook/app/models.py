from django.db import models

# Create your models here.
class Book(models.Model):
    nid = models.AutoField(primary_key=True)
    title = models.CharField(max_length=64)
    score = models.CharField(max_length=32)
    img_src = models.CharField(max_length=64, null=True)
    publish_detail = models.CharField(max_length=255)
    slogan = models.CharField(max_length=255)
    publish_house = models.CharField(max_length=32, null=True)
    detail_addr = models.CharField(max_length=255, null=True)
    comment_people = models.CharField(max_length=32, null=True)
    publishing_house = models.ForeignKey(to="Publishing_house",to_field="nid",on_delete=models.CASCADE, null=True)
    def __str__(self):
        return self.title
    

class Publishing_house(models.Model):
    nid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name
    