from django.http import HttpResponse
from django.shortcuts import render

# 导入分页器
from django.core.paginator import Paginator,EmptyPage
# Create your views here.
from app.models import Book,Publishing_house

def publish_house_info_1(request):
    from django.db.models import Avg
    house_list = Publishing_house.objects.all().annotate(c=Avg("book__score")).order_by('-c')
    for house in house_list:
        print(house.name, '平均得分为：',house.c)

    paginator = Paginator(house_list, 6)
    current_page_num = int(request.GET.get("page", 1))

    if paginator.num_pages > 11:
        if current_page_num - 5 < 1:
            page_range = range(1,12)
        elif current_page_num + 5 > paginator.num_pages:
            page_range = range(paginator.num_pages-10,paginator.num_pages+1)
        else: 
            page_range = range(current_page_num-5,current_page_num+6)
    else:
        page_range = paginator.page_range

    try:
        current_page = paginator.page(current_page_num)  # 当前页
    except EmptyPage as e:
        current_page = paginator.page(1)
    return render(request, "app/publish_house_info_1.html", locals())

def publish_house_info_2(request):
    from django.db.models import Count
    from django.db.models import Sum
    house_list = Publishing_house.objects.all().annotate(c=Sum("book__comment_people")).order_by('-c')
    for house in house_list:
        house.c = int(house.c)
        print(house.name, '平均评价人次为：', house.c)

    paginator = Paginator(house_list, 6)
    current_page_num = int(request.GET.get("page", 1))

    if paginator.num_pages > 11:
        if current_page_num - 5 < 1:
            page_range = range(1,12)
        elif current_page_num + 5 > paginator.num_pages:
            page_range = range(paginator.num_pages-10,paginator.num_pages+1)
        else: 
            page_range = range(current_page_num-5,current_page_num+6)
    else:
        page_range = paginator.page_range

    try:
        current_page = paginator.page(current_page_num)  # 当前页
    except EmptyPage as e:
        current_page = paginator.page(1)
    return render(request, "app/publish_house_info_2.html", locals())

def publish_house_info_3(request):
    from django.db.models import Count
    house_list = Publishing_house.objects.all().annotate(c=Count("book__title")).order_by('-c')

    paginator = Paginator(house_list, 6)
    current_page_num = int(request.GET.get("page", 1))

    if paginator.num_pages > 11:
        if current_page_num - 5 < 1:
            page_range = range(1,12)
        elif current_page_num + 5 > paginator.num_pages:
            page_range = range(paginator.num_pages-10,paginator.num_pages+1)
        else: 
            page_range = range(current_page_num-5,current_page_num+6)
    else:
        page_range = paginator.page_range

    try:
        current_page = paginator.page(current_page_num)  # 当前页
    except EmptyPage as e:
        current_page = paginator.page(1)
    return render(request, "app/publish_house_info_3.html", locals())

def publish_search(request):
    from django.db.models import Count
    if request.method == 'POST':
        housename = request.POST.get('housename')
        house_get = Publishing_house.objects.filter(name=housename).first()
        book_list = house_get.book_set.all()   
        if house_get:
            return render(request,"app/publish_search.html",{'house':house_get, 'book_list':book_list})
        else:
            notfound = '没找到...  试试查找别的出版社吧（‐＾▽＾‐）'
            return render(request,"app/publish_search.html",{'notfound':notfound})
    else:
        notfound = '请输入出版社名称...'
        return render(request,"app/publish_search.html",{'notfound':notfound})    


def bar_chart(request):
    print('柱状图信息：')
    from django.db.models import Count
    book_num = Publishing_house.objects.all().annotate(c=Count("book__title")).order_by('-c')
    for i in range(11):
        print(book_num[i].name, '共',book_num[i].c,'本书')
    return render(request, "app/bar_chart.html", {})

def bar_chart_2(request):
    print('柱状图信息：')
    from django.db.models import Count
    from django.db.models import Avg 
    book_num = Publishing_house.objects.all().annotate(c=Count("book__title")).order_by('-c')
    for i in range(11):
        score_avg = Book.objects.filter(publish_house=book_num[i].name).aggregate(c=Avg("score"))
        print(book_num[i].name, '共',book_num[i].c,'本书', '平均得分为：',score_avg)
    return render(request, "app/bar_chart_2.html", {})

def pie_graph(request):
    print('饼状图信息:')
    book_list = Book.objects.all()
    num = 0
    for book in book_list:
        num = num + int(book.comment_people)
    print('总评价人次:',num)
    from django.db.models import Avg    
    from django.db.models import Count
    book_num = Publishing_house.objects.all().annotate(c=Count("book__title")).order_by('-c')
    for i in range(5):
        score_avg = Book.objects.filter(publish_house=book_num[i].name).aggregate(c=Avg("score"))
        ordered_book = Book.objects.filter(publish_house=book_num[i].name).aggregate(x=Avg("comment_people"))
        print(book_num[i].name, '共',book_num[i].c,'本书', '平均分:',score_avg, '总评价人次：', ordered_book['x']*book_num[i].c)

    return render(request, "app/pie_graph.html", {})

def book_search(request):
    if request.method == 'POST':
        bookname = request.POST.get('bookname')
        # book_list = Book.objects.all()
        book_get = Book.objects.filter(title=bookname).first()
        if book_get:
            return render(request,"app/book_search.html",{'book':book_get})
        else:
            notfound = '没找到...  试试查找别的书籍吧（‐＾▽＾‐）'
            return render(request,"app/book_search.html",{'notfound':notfound})
    else:
        notfound = '请输入书籍名称...'
        return render(request,"app/book_search.html",{'notfound':notfound})    



def index(request):

    book_list = Book.objects.all()

    paginator = Paginator(book_list, 6)

    current_page_num = int(request.GET.get("page", 1))

    if paginator.num_pages > 11:
        if current_page_num - 5 < 1:
            page_range = range(1,12)
        elif current_page_num + 5 > paginator.num_pages:
            page_range = range(paginator.num_pages-10,paginator.num_pages+1)
        else: 
            page_range = range(current_page_num-5,current_page_num+6)
    else:
        page_range = paginator.page_range

    try:

        current_page = paginator.page(current_page_num)  # 当前页

    except EmptyPage as e:
        current_page = paginator.page(1)

    return render(request,"app/index.html",locals())